from picamera import PiCamera, Color
from time import sleep
from datetime import datetime


camera = PiCamera()



# Take two still pictures, and save them.
# The 3 sec sleep is to let the camera adjust its light levels
# To view an image from the command line on the RPi: xdg-open image1.jpg

camera.start_preview()
camera.annotate_text_size = 35
camera.annotate_background = Color('black')
camera.annotate_foreground = Color('white')
for n in range(2):
    sleep(3)
    camera.annotate_text = str(datetime.now())
    camera.capture("/home/pi/Desktop/image{n}.jpg".format(n=n))
camera.stop_preview()




# Record a video of 10 sec
# To view the video from the command line on the RPi: omxplayer video.h264

camera.annotate_text = ''
camera.start_preview()
camera.start_recording("/home/pi/Desktop/video.h264")
sleep(10)
camera.stop_recording()
camera.stop_preview()

