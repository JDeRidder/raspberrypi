
from picamera import PiCamera
from picamera.array import PiRGBArray
import datetime
import imutils
import time
import cv2
 

# Initialize the camera

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 16

# Allow the camera to warmup

time.sleep(3.0)


# Create a named window in OpenCv 
# The startWindowThread() is required when running this script through an
# interactive IPython session.

cv2.startWindowThread()
cv2.namedWindow("Video stream")



runningAverageImage = None
startingTimeOfSeriesOfFramesWithMovingObject = datetime.datetime.now()
numFramesWithMovingObject = 0

# capture frames from the camera

try:
    capturedFrame = PiRGBArray(camera, size=(640, 480))
    for f in camera.capture_continuous(capturedFrame, format="bgr", use_video_port=True):

        frame = f.array
        timestamp = datetime.datetime.now()
 
        # Resize the frame, convert it to grayscale, and smooth it.
        # The smoothing is to avoid small changes in the frame to cause an alert.

        resizedFrame = imutils.resize(frame, width=500)
        grayImage = cv2.cvtColor(resizedFrame, cv2.COLOR_BGR2GRAY)
        smoothedImage = cv2.GaussianBlur(grayImage, (21, 21), 0)

        # If we don't have a running average image yet, make one.
        # Then clear the stream for the next frame.

        if runningAverageImage is None:
            runningAverageImage = smoothedImage.copy().astype("float")
            capturedFrame.truncate(0)
            continue
 
        # If we did have a running average image, update it. alpha=0.5 means
        #     average image = (1-alpha) * average image + alpha * current image

        cv2.accumulateWeighted(smoothedImage, runningAverageImage, 0.5)

        # Compute difference between the current and the historic image
        # The convertScaleAbs is to convert from float to unsigned 8-bit.

        deltaFrame = cv2.absdiff(smoothedImage, cv2.convertScaleAbs(runningAverageImage))

        # We clean/smooth the delta frame in two ways:
        # 1) We zero every element below a specified threshold
        # 2) We dilate the image, meaning that we replace every pixel by the maximum pixel
        #    value in its neighbourhood. This smooths and fills up any holes in the deltaFrame.

        threshold = 5
        thresholdedDeltaFrame = cv2.threshold(deltaFrame, threshold, 255, cv2.THRESH_BINARY)[1]
        dilatedDeltaFrame = cv2.dilate(thresholdedDeltaFrame, None, iterations=2)

        # Find the contours around the moving object that shows up in the deltaFrame
        
        contours = cv2.findContours(dilatedDeltaFrame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)
        
        # Loop over the contours
       
        motionDetected = False
        for contour in contours:
                
            # If the area within the contour is too small (say 50x50 = 2500 pixels): ignore it.
            
            contourMinimalArea = 2500
            if cv2.contourArea(contour) < contourMinimalArea:
                continue
 
            # Compute the bounding box for the contour, and draw it on the original frame

            (x, y, w, h) = cv2.boundingRect(contour)
            cv2.rectangle(resizedFrame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            
            # Since we have a large enough contour, this means we detected a moving object

            motionDetected = True

        # Timestamp the original image

        text = timestamp.strftime("%d-%m-%Y %H:%M:%S")
        cv2.putText(resizedFrame, text, (10, resizedFrame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

        # We only trust the motion detection if we can detect uninterruptedly at least 
        # 4 frames with an object in motion, with each of these frames 1 second apart.
        
        detectionTimeInterval = 1.0            # seconds
        minNumOfFramesWithMovingObjects = 4

        if motionDetected == True:
            if numFramesWithMovingObject == 0:
                startingTimeOfSeriesOfFramesWithMovingObject = timestamp
                numFramesWithMovingObject += 1
            else:
                if (timestamp - startingTimeOfSeriesOfFramesWithMovingObject).seconds >= detectionTimeInterval * (numFramesWithMovingObject+1):
                    numFramesWithMovingObject += 1
                if numFramesWithMovingObject >= minNumOfFramesWithMovingObjects:
                    print("Consistent motion detected!")
                    
                    # Reset everything for a new series. 
                    
                    startingTimeOfSeriesOfFramesWithMovingObject = timestamp
                    numFramesWithMovingObject = 0
        else:
            # We no longer detected a moving object, so reset the counter

            numFramesWithMovingObject = 0

        # Display the security feed

        cv2.imshow("Video Stream", resizedFrame)
        key = cv2.waitKey(1) 
 
        # Clear the stream for the next frame
        
        capturedFrame.truncate(0)

# Allow the user to interrupt with Ctrl-c

except Exception as e:
    if hasattr(e, 'message'):
        print(e.message)
finally:
    camera.close()
    cv2.destroyAllWindows()
