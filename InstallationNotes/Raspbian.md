# Installation and Configuration of Raspbian

## Format SD card

* SD card of at least 32 GB, class 10 or better
* In `Disk Utility` (on Mac):
    * choose Erase
    * Format: MS-DOS FAT-32
    * Name: (e.g.) Raspberry

## Copy Raspbian Image to SD card

* Download [latest image](https://www.raspberrypi.org/downloads/raspbian/) of Raspbian (with the recommended software)
* Copy Raspbian image to the SD card using [Etcher](https://www.balena.io/etcher/) (Both Mac & Windows, admin pw required)
    * Name of the SD partition is now `Boot`

## Boot the Raspberry Pi

* Plug in the SD card, a power cord, a USB keyboard, a USB mouse, and an HDMI screen into the RPi
* Follow setup wizzard (Change password, WiFi, Language, ...)

## Raspberry Pi Configuration

Raspberry Menu -> Preferences -> Raspberry Pi Configuration

* System
    * Hostname
* Interfaces
    * Enable Camera
    * Enable SSH
    * Enable VNC
* Localisation
    * Timezone
    * Keyboard
    * WiFi country code

**Note**: You may need to load the correct video 4 linux drivers for the camera to work.

        sudo modprobe bcm2835-v4l2

## Installing a package manager 
* sudo apt-get install synaptic
* Launch in terminal with `sudo synaptic`
* Launch in menu: Preferences -> Synaptic Package manager

## Remote access

* Give a static IP address to the raspberry (easier to use with VSCode):
    * `ifconfig`: Note the IP address that follows `inet`
    * Alternatively: 
        
            ifconfig wlan0 | grep inet | awk '{print $2}'

    * Get your router�s gateway address:
        * `route -n`, under the heading Gateway
    * `sudo vim /etc/dhcpcd.conf`, scroll down to bottom change to e.g.:
        
            interface eth0
            static ip_address=192.168.1.4
            static routers=192.168.1.1
            static domain_name_servers=8.8.8.8
        
    * `sudo reboot`
* `ssh pi@raspberry`


## See also

* [Installation documentation](https://www.raspberrypi.org/documentation/installation/) on raspberrypi.org
* [Codonet blog on static IP address](https://www.codedonut.com/linux/give-static-ip-address-raspberry-pi/)
