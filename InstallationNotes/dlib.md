# Installation of dlib

Dlib is a modern C++ toolkit containing machine learning algorithms.

* Clone the repository from Github

        cd ~/Development
        git clone https://github.com/davisking/dlib.git dlib

* Build the library

        cd dlib
        mkdir build
        cd build
        cmake ..
        cmake --build .

    This is on one core only, so rather slow. The result is in `build/dlib/libdlib.a`.

* Install the library

        sudo make install

* Increase the Swap space

    When trying to build the Python API, I got the following error:

        C++: internal compiler error: Killed (program cc1plus)

    which signals a memory problem. To mitigate, we increase the swap space size to add more memory. Use vim to set `CONF_SWAPSIZE=1024`:
  
        sudo vim /etc/dphys-swapfile
  
    Then enforce it with:
  
        sudo /etc/init.d/dphys-swapfile stop
        sudo /etc/init.d/dphys-swapfile start

    Verify with:

        free -m
    
* Install the Python package `skbuild` required by Dlib's `setup.py`. 

        pip3 install scikit-build

* Build the Python API

        cd ~/Development/dlib
        sudo python3 setup.py install

  **Note**: There was an `ImportError: No module named 'skbuild'` despite the fact that `import skbuild` works perfectly fine. However, it doesn't seem to have any effect on the installation. An example program using `import dlib` works fine.

* Change back the Swap size to `CONF_SWAPSIZE=100`
  
        sudo vim /etc/dphys-swapfile
        sudo /etc/init.d/dphys-swapfile stop
        sudo /etc/init.d/dphys-swapfile start

  An SD card has a limited number of writes, before it will be corrupted. A too large Swap size will therefore lower the SD cards life expectancy. Backup your .img file, after increasing the Swap size to install a library.

## See also:

* [The Github repository](https://github.com/davisking/dlib) of Dlib
* [Dlib's documentation](http://dlib.net)
* [JackyLe's installation instructions](https://www.jackyle.com/2017/11/install-dlib-on-raspberry-pi.html)