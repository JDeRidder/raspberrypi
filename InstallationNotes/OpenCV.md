# Installation of OpenCV

* Upgrade packages and reboot
  
        sudo apt-get update 
        sudo apt-get upgrade
        sudo reboot

* Install cmake
  
        sudo apt-get install build-essential cmake pkg-config
  
* Install image I/O libraries
  
        sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev

* Install video I/O libraries
  
        sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
        sudo apt-get install libxvidcore-dev libx264-dev

* Install gtk dev library to enable OpenCV's tool to display images
  
        sudo apt-get install libgtk2.0-dev libgtk-3-dev
  
* Install ATLAS and gfortran to optimize OpenCV
  
        sudo apt-get install libatlas-base-dev gfortran
  
* Download OpenCV 4.0.0
  
        cd ~
        wget -O opencv.zip https://github.com/opencv/opencv/archive/4.0.0.zip
        unzip opencv.zip
        wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.0.0.zip
        unzip opencv_contrib.zip
  
* Generate the Makefile using cmake
  
        cd opencv4.0.0
        mkdir build
        cd build
        cmake -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=/usr/local \
        -D BUILD_opencv_java=OFF \
        -D BUILD_opencv_python2=OFF \
        -D BUILD_opencv_python3=ON \
        -D PYTHON_DEFAULT_EXECUTABLE=$(which python3) \
        -D INSTALL_C_EXAMPLES=OFF \
        -D INSTALL_PYTHON_EXAMPLES=ON \
        -D BUILD_EXAMPLES=ON\
        -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-4.0.0/modules \
        -D WITH_CUDA=OFF \
        -D BUILD_TESTS=OFF \
        -D BUILD_PERF_TESTS= OFF ..
    We assumed here that you already have `python3` installed. 

* Increase the swap space size to add more virtual memory, so that we can compile with 4 cores rather than 1. Use vim to set `CONF_SWAPSIZE=1024`:
  
        sudo vim /etc/dphys-swapfile
  
  Then enforce it with:
  
        sudo /etc/init.d/dphys-swapfile stop
        sudo /etc/init.d/dphys-swapfile start
  
* Compile and Install OpenCV

        make -j4
        sudo make install
        sudo ldconfig

  This takes a long time, and the RPi processor can get quite hot. 
  
* For a reason that I don't understand yet, the `cv2.so` library was not properly installed, so I had to copy it manually:
  
        sudo cp ~/opencv-4.0.0/build/lib/python3/cv2.cpython-35m-arm-linux-gnueabihf.so /usr/local/lib/python3.5/dist-packages/cv2.so
  
* Test Python access to OpenCV
  
        python3 -c 'import cv2; print(cv2.__version__)'
  
* Change back the Swap size to `CONF_SWAPSIZE=100`
  
        sudo vim /etc/dphys-swapfile
        sudo /etc/init.d/dphys-swapfile stop
        sudo /etc/init.d/dphys-swapfile start
  
* Remove the OpenCV zip files

        cd ~
        rm opencv.zip opencv_contrib.zip
  



## See also

* [Life2Coding's installation instructions](https://www.life2coding.com/install-opencv-3-4-0-python-3-raspberry-pi-3/)