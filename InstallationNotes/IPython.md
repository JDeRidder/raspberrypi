# Installing IPython

* Using pip3

        pip3 install ipython

* Add in .bashrc ...
        
        alias ipython="python3 -m IPython"

* ... and let the alias take effect:

        source .bashrc